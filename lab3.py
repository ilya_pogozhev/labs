import math
import matplotlib.pyplot as plt

# Функция, реализующая основную программу
def mycode():
    x_lst = []
    y_lst = []
    try:
        a = float(input("Введите а "))
        x = float(input("Введите x  "))
        x_max = float(input("Введите x максимальное "))
        command = int(input("Введите команду 1-g, 2-f, 3-y. "))
        number = int(input("Количество шагов вычисления "))
        stride = float(input("Величина шагов вычисления "))
    except ValueError:
        print("Ввод некорректен")
        return 1

    step = 0 # подсчет шагов

    for i in range(number):
        if command == 1:
            try:
                g = (5 * (10 * a ** 2 - 11 * a * x + x ** 2)) / (24 * a ** 2 - 49 * a * x + 15 * x ** 2)
                x += stride
                step += 1
                print(step,"x=",x, "G=",g)
                x_lst.append(x)
                y_lst.append(g)
                if x >= x_max:
                    print("Максимум х достигнут")
                    break

            except ZeroDivisionError:
                g = None
                x += stride
                step += 1
                print(step, "x=", x, "G=", g)
                if x >= x_max:
                    print("Максимум х достигнут")
                    break

        elif command == 2:
            try:
                f = math.sinh(3 * a ** 2 + 7 * a * x + 4 * x ** 2)  # гиперболический синус
                x += stride
                step += 1
                print(step,"x=",x, "F=",f)
                x_lst.append(x)
                y_lst.append(f)
                if x >= x_max:
                    print("Максимум х достигнут")
                    break

            except ValueError:
                f = None
                x += stride
                step += 1
                print(step, "x=", x, "F=", f)
                if x >= x_max:
                    print("Максимум х достигнут")
                    break

        elif command == 3:
            try:
                
                y = -math.atanh(30 * a ** 2 + 37 * a * x - 4 * x ** 2)  # гиперболический арктангенс
                x += stride
                step += 1
                print(step,"x=",x, "Y=",y)
                x_lst.append(x)
                y_lst.append(y)
                if x >= x_max:
                    print("Максимум х достигнут")
                    break

            except ValueError:
                y = None
                x += stride
                step += 1
                print(step, "x=", x, "Y=", y)
                if x >= x_max:
                    print("Максимум х достигнут")
                    break
        else:
            print("Комманда не выбрана или выбрана неправильно")

    plt.xlabel('x --->')
    plt.ylabel('f(x) --->')
    plt.title('График функции')
    plt.plot(x_lst, y_lst, 'r.-')
    plt.show()

while True or OverflowError:
    mycode()
    again = input("Хотите начать программу сначала? [Y/N]: ")
    if again not in ["Y", "y", "Д", "д", "Да", "да"]:
        break