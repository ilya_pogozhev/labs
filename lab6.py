import math


# Функция, реализующая основную программу
def mycode():
    g_lst = []
    f_lst = []
    y_lst = []

    try:
        data = list(map(float, input(
            'Введите через пробел а,x,х максимальное,число шагов ').split()))
        a = int(data[0])
        x = int(data[1])
        x_max = int(data[2])
        number = int(data[3])
        stride = (x_max - x) / number

    except ValueError:
        print("Ввод некорректен")
        return 1

    for i in range(number):
        try:
            g = (5 * (10 * a ** 2 - 11 * a * x + x ** 2)) / (24 * a ** 2 - 49 * a * x + 15 * x ** 2)
            g_lst.append(g)
            f = math.sinh(3 * a ** 2 + 7 * a * x + 4 * x ** 2)  # гиперболический синус
            f_lst.append(f)
            y = -math.atanh(30 * a ** 2 + 37 * a * x - 4 * x ** 2)  # гиперболический арктангенс
            y_lst.append(y)

        except ZeroDivisionError:
            g = None
            g_lst.append(g)
            f = None
            f_lst.append(f)

        except ValueError:
            y = None
            y_lst.append(y)
        x += stride

    for g_lst, y_lst, f_lst in zip(g_lst,y_lst,f_lst):
        print(f'{g_lst}\t{y_lst}\t{f_lst}')

while True or OverflowError:
    mycode()
    again = input("Хотите начать программу сначала? [Y/N]: ")
    if again not in ["Y", "y", "Д", "д", "Да", "да"]:
        break