import lib_for_kr5
import matplotlib.pyplot as plt
import random
import sys
import timeit

size_data = []
time_data = []
n_data = []

time_lst_1 = []
time_lst_2 = []
matrix_size_list = []



for n in range(10, 100, 10):
    mat1 = [[random.uniform(-10, 10) for j in range(n)] for i in range(n)]
    mat2 = [random.uniform(-10, 10) for i in range(n)]
    averages_time = []
    averages_size = []
    for _ in range(3):
        start = timeit.default_timer()
        averages_size.append(sys.getsizeof(lib_for_kr5.gauss_jordan(mat1, mat2)))
        averages_time.append(timeit.default_timer() - start)
    time_data.append(sum(averages_time) / 3)
    size_data.append(sum(averages_size) / 3)
    print(time_data[-1])
    n_data.append(n)

    averages_size.clear()

    for _ in range(3):
        time_temp = 0
        averages_size = []
        start = timeit.default_timer()
        r, m = n, mat1.copy()
        result_vector = lib_for_kr5.cramer_calculation(r, m)
        matrix_size = sys.getsizeof(result_vector)
        averages_size.append(matrix_size)
        end = timeit.default_timer()
        time_temp += end - start


    time_lst_2.append(float(time_temp) / 3)
    matrix_size_list.append(sum(averages_size) / 3)
    print(time_lst_2[-1])

plt.plot( n_data, time_data, "ro-", c='red')
plt.xlabel('длина матриц')
plt.ylabel('время выполнения, с')

plt.show()

plt.plot(n_data, size_data, "ro-", c='black')
plt.xlabel('длина матриц')
plt.ylabel('занимаемая память, байт')

plt.show()

plt.plot(n_data, time_lst_2, "ro-", c='black')
plt.xlabel('длина матриц')
plt.ylabel('время выполнения, с')
plt.show()

plt.plot( n_data, matrix_size_list, "ro-", c='green')
plt.xlabel('длина матриц')
plt.ylabel('занимаемая память, байт')
plt.show()





