from lib_for_kr5 import gauss_jordan_calculation, cramer_calculation


number = int(input('Введите количество матриц\t'))
matrix_lst = []

print("Заполните ячейки матрицы:")
for string in range(number):
    matrix_row = []
    for column in range(number + 1):
        matrix_row.append(float(input(f"Введите ненулевое значение ячейки matrix[{string}][{column}]:\t")))
    matrix_lst.append(matrix_row.copy())

choice_method = input("Выберите метод решения системы уравнений - Gauss-Jordan (1) или Cramer (2):\t")
if choice_method == "1":
    result_vector = gauss_jordan_calculation(number, matrix_lst)
else:
    result_vector = cramer_calculation(number, matrix_lst)
for i in range(number):
    print(f"X{i} = {result_vector[i]}", end='\t')