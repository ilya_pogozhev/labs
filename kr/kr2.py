import time
import os


os.system('')
elements = 0
lst_earth = []


while True:
    try:
        elements = input('')
        if elements.startswith('```') or elements.startswith('\n'):
            continue
        lst_earth.append(elements)
    except EOFError:
        break


for i in lst_earth:
    if i.startswith("#"):
        i = ''
        print('\033c', end="")
        time.sleep(0.5)
    print(f"\033[32m {i}")