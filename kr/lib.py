def vector_add(a, x: list, y: list):
    if len(x) == len(y):
        for i in range(len(x)):
            x[i] = a * x[i]
        for i in range(len(y)):
            y[i] = y[i] + x[i]
        return y
    else:
        return None