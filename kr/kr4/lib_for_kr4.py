import matplotlib.pyplot as plt
import numpy as np

# Алгоритм SAXPY

def matrix_calculation(matrix_a, matrix_b):
    matrix_b = list(zip(*matrix_b))
    return [
        [
            sum(x_coord * y_coord for x_coord, y_coord in zip(matrix_a_row, matrix_b_column))
            for matrix_b_column in matrix_b
        ]
        for matrix_a_row in matrix_a
    ]







