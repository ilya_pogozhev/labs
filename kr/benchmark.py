import lib
import time
import random
import matplotlib.pyplot as plt

n_data = []
time_data = []
n_data2 = []
time_data2 = []

a = 10.5

for n in range(int(10e4), int(10e5), int(10e4)):
    time_list = []
    vect = [random.randint(-10, 10) for i in range(n)]
    for i in range(3):
        start = time.time()
        lib.vector_add(a, vect, vect)
        time_list.append(time.time() - start)
    time_num = sum(time_list) / 3
    print(time_num)
    n_data.append(n)
    time_data.append(time_num)
time_list.clear()

for n in range(int(10e4), int(10e5), int(10e4)):
    time_list = []
    vect = [random.uniform(-10, 10) for i in range(n)]
    for i in range(3):
        start = time.time()
        lib.vector_add(a, vect, vect)
        time_list.append(time.time() - start)
    time_num = sum(time_list) / 3
    print(time_num)
    n_data2.append(n)
    time_data2.append(time_num)


plt.plot(n_data, time_data, 'o', c='red')
plt.plot(n_data, time_data, 'b', c='red')

plt.plot(n_data2, time_data2, 'o', c='black')
plt.plot(n_data2, time_data2, 'b', c='black')
plt.title('График зависимости длины вектора от времени ')
plt.xlabel('Длина вектора')
plt.ylabel('Время, с')
plt.show()